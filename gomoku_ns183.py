#!/usr/bin/python3

N = 15

# The main routine of AI.
# input: str[N][N] field : state of the field.
# output: int[2] : where to put a stone in this turn.
def Think(field):
    CENTER = (int(N / 2), int(N / 2))

    best_position = (0, 0)
    best_position_rank = 5
    for i in range(N):
        for j in range(N):


            if field[i][j] != '.':
                continue
            position = (i, j)


            # Assume to put a stone on (i, j).
            field[i][j] = 'O'
            if JudgeTheValueOfPosition(field, position, 'O')==4:
                DebugPrint('I have a winning choice at (%d, %d)' % (i, j))
                return position

            field[i][j] = 'X'
            if JudgeTheValueOfPosition(field, position, 'X')==4:
                return position

            elif JudgeTheValueOfPosition(field, position, 'X')==3:
                best_position = position
                best_position_rank = 1

            elif JudgeTheValueOfPosition(field, position, 'X')==2:
                if best_position_rank >= 2:
                    best_position = position
                    best_position_rank = 2





            field[i][j] = 'O'
            if JudgeTheValueOfPosition(field, position, 'O')==3:
                if best_position_rank>=3:
                  best_position = position
                  best_position_rank = 3


            # Revert the assumption.
            field[i][j] = '.'
            if GetDistance(best_position, CENTER) > GetDistance(position, CENTER):
                if best_position_rank>=4:
                    best_position = position
                    best_position_rank = 4
    return best_position


def JudgeTheValueOfPosition(field, position, mark):
	theNumOfStones = [0,0,0,0]
	theNumOfStones[0] =  CountMarkedStonesOnLine(field, position, (1, 1), mark)
	theNumOfStones[1] =  CountMarkedStonesOnLine(field, position, (1, 0), mark)
	theNumOfStones[2] =  CountMarkedStonesOnLine(field, position, (1, -1), mark)
	theNumOfStones[3] =  CountMarkedStonesOnLine(field, position, (0, 1), mark)

	if (theNumOfStones[0]>=5 or theNumOfStones[1]>=5 or theNumOfStones[2]>=5 or theNumOfStones[3]>=5) :
		return 4
	elif (theNumOfStones[0]==4 or theNumOfStones[1]==4 or theNumOfStones[2]==4 or theNumOfStones[3]==4) :
		return 3
	else:
		count=0
		if theNumOfStones[0] == 3:
			count+=1
		if theNumOfStones[1] == 3:
			count+=1
		if theNumOfStones[2] == 3:
			count+=1
		if theNumOfStones[3] == 3:
			count+=1
		if count>=2:
			return 2
		else:
			return 0

def CountMarkedStonesOnLine(field, position, diff, mark):
    count = 0
    row = position[0]
    col = position[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != mark:
            break
        row += diff[0]
        col += diff[1]
        count += 1
    row = position[0] - diff[0]
    col = position[1] - diff[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != mark:
            break
        row -= diff[0]
        col -= diff[1]
        count += 1
    return count


# Returns the Manhattan distance from |a| to |b|.
def GetDistance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


# =============================================================================
# DO NOT EDIT FOLLOWING FUNCTIONS
# =============================================================================

def main():
    field = Input()
    position = Think(field)
    Output(position)


def Input():
    field = [list(input()) for i in range(N)]
    return field


def Output(position):
    print(position[0], position[1])


# Outputs |msg| to stderr; This is actually a thin wrapper of print().
def DebugPrint(*msg):
    import sys
    print(*msg, file=sys.stderr)


if __name__    == '__main__':
    main()
