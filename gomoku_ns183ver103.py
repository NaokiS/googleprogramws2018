#!/usr/bin/python3

N = 15

# The main routine of AI.
# input: str[N][N] field : state of the field.
# output: int[2] : where to put a stone in this turn.
def Think(field):
    CENTER = (int(N / 2), int(N / 2))

    best_position = (0, 0)
    best_position_rank = 5
    for i in range(N):
        for j in range(N):

            
            if field[i][j] != '.':
                continue
            position = (i, j)


            # Assume to put a stone on (i, j).
            field[i][j] = 'O'
            if CanHaveFourOrFiveStones(field, position)==4:
                DebugPrint('I have a winning choice at (%d, %d)' % (i, j))
                return position

            field[i][j] = 'X'
            if CanRivalHaveFourOrFiveStones(field,position)==4:
                return position

            elif CanRivalHaveFourOrFiveStones(field,position)==3:   
                best_position = position
                best_position_rank = 1

            elif CanRivalHaveFourOrFiveStones(field,position)==2:   
                if best_position_rank >= 2:
                    best_position = position
                    best_position_rank = 2





            field[i][j] = 'O'
            if CanHaveFourOrFiveStones(field, position)==3:
                if best_position_rank>=3:
                  best_position = position
                  best_position_rank = 3


            # Revert the assumption.
            field[i][j] = '.'
            if GetDistance(best_position, CENTER) > GetDistance(position, CENTER):
                if best_position_rank>=4:
                    best_position = position
                    best_position_rank = 4
    return best_position



def CanRivalHaveFourOrFiveStones(field, position):
    if (CountRivalStonesOnLine(field, position, (1, 1)) >= 5 or
                    CountRivalStonesOnLine(field, position, (1, 0)) >= 5 or
                                CountRivalStonesOnLine(field, position, (1, -1)) >= 5 or
                                CountRivalStonesOnLine(field, position, (0, 1)) >= 5) :
        return 4
    elif (CountRivalStonesOnLine(field, position, (1, 1)) == 4 or
                CountRivalStonesOnLine(field, position, (1, 0)) == 4 or
                                CountRivalStonesOnLine(field, position, (1, -1)) == 4 or
                                CountRivalStonesOnLine(field, position, (0, 1)) == 4) :
        return 3
    else:
        count=0;
        if  CountRivalStonesOnLine(field, position, (1, 1)) == 3:
            count +=1
        if  CountRivalStonesOnLine(field, position, (1, 0)) == 3:
            count +=1
        if  CountRivalStonesOnLine(field, position, (1, -1)) == 3:
            count +=1
        if  CountRivalStonesOnLine(field, position, (0, 1)) == 3:
            count +=1
        if count>=2:
            return 2
        return 0

def CountRivalStonesOnLine(field, position, diff):
    count = 0
    row = position[0]
    col = position[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != 'X':
            break
        row += diff[0]
        col += diff[1]
        count += 1
    row = position[0] - diff[0]
    col = position[1] - diff[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != 'X':
            break
        row -= diff[0]
        col -= diff[1]
        count += 1
    return count

# Returns true if you have five stones from |position|. Returns false otherwise.
def CanHaveFourOrFiveStones(field, position):
    if (CountStonesOnLine(field, position, (1, 1)) >= 5 or
                    CountStonesOnLine(field, position, (1, 0)) >= 5 or
                                CountStonesOnLine(field, position, (1, -1)) >= 5 or
                                CountStonesOnLine(field, position, (0, 1)) >= 5) :
        return 4
    elif (CountStonesOnLine(field, position, (1, 1)) == 4 or
                    CountStonesOnLine(field, position, (1, 0)) == 4 or
                                CountStonesOnLine(field, position, (1, -1)) == 4 or
                                CountStonesOnLine(field, position, (0, 1)) ==4) :
        return 3
    else:
        return 0



# Returns the number of stones you can put around |position| in the direction specified by |diff|.
def CountStonesOnLine(field, position, diff):
    count = 0

    row = position[0]
    col = position[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != 'O':
            break
        row += diff[0]
        col += diff[1]
        count += 1

    row = position[0] - diff[0]
    col = position[1] - diff[1]
    while True:
        if row < 0 or col < 0 or row >= N or col >= N or field[row][col] != 'O':
            break
        row -= diff[0]
        col -= diff[1]
        count += 1

    return count


# Returns the Manhattan distance from |a| to |b|.
def GetDistance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])


# =============================================================================
# DO NOT EDIT FOLLOWING FUNCTIONS
# =============================================================================

def main():
    field = Input()
    position = Think(field)
    Output(position)


def Input():
    field = [list(input()) for i in range(N)]
    return field


def Output(position):
    print(position[0], position[1])


# Outputs |msg| to stderr; This is actually a thin wrapper of print().
def DebugPrint(*msg):
    import sys
    print(*msg, file=sys.stderr)


if __name__    == '__main__':
    main()
